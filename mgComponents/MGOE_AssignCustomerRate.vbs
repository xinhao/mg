﻿Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
strCoverStep = "Assign Customer Rate"

' ### Wait until an element exist, can't use "Loading..." label becuase its "visible" is always True
Function WaitForRadioButton(intTimeOut) ' by check if radio buttons are present
   Dim i
	i = intTimeOut
   Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//div[contains(@class, 'mg-grid-row-radio')])[1]").Exist(0)) and i > 0
	   Wait 1
	   i = i - 1
   Loop
   WaitForRadioButton = i
'   Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[starts-with(.='ICS DRY VAN PUBLISHED']").Exist 
End Function

' There are multiple Loading... elements, currently the last() one is for the Rating window and no certainty to idnetify by other dynamic attributes
'  When loading is done, the Loading... element's style changes from not containing "display: none"  to containing it. xpath:=//*[@style] doesn't work
' Looking for the last Loading... element Object.style.display value exists and equals to 'none'
Function WaitForLastLoadingComplete(intTimeOut)
	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "//div[./div/div[normalize-space(.)='Loading...']]"

	' ### Make sure the Rating screen pops up
	strRatingXpath = "xpath:=//div[./div[contains(@id, 'frame1ML')]/div/div/div[contains(@id, 'header-body')]/div/div[contains(@id, 'header-targetEl')]/div/span[.='Rating']]"
	Set objRating = objPageMG.Frame("name:=Detail").WebElement(strRatingXpath)
	Do
		Wait 1
	Loop Until objRating.Exist(1)

	' ### Then check if Loading... disappears
	strElement  = "xpath:=" & strXpath
	strElement1 = "xpath:=(" & strXpath & ")[1]"
	strDisplay = ""
	i = intTimeOut
	Do While strDisplay = "" and i > 0
		Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
		Set objElement1 = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement1)
		If  objElement.Exist(0) Then
			strDisplay = objElement.Object.style.display
		ElseIf  objElement1.Exist(0) Then
			Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).Webelement("xpath:=(" & strXpath & ")[last()]")
			strDisplay = objElement.Object.style.display
		Else
			strDisplay = "none"
		End If
		i = i - 1
	Loop
	WaitForLastLoadingComplete = i
'	msgbox "loading complete"
End Function

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub
'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'' look for the load on Master Load Board
'' first set Created By Me
'objPageMG.Sync
'intWaitSeconds = 0
'Do
'	objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Set "" 'works
'	wait intWaitSeconds
'	objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Click
'	wait intWaitSeconds
'	objWS.SendKeys "Created"
'	wait intWaitSeconds
'	Reporter.ReportEvent micDone, "intWaitSeconds", intWaitSeconds
'	intWaitSeconds = intWaitSeconds + 1
'Loop Until objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Exist Or intWaitSeconds > 5
'
'If intWaitSeconds <= 5 Then
'	objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").WaitProperty "visible", "True"
'	objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Click 'working
'	Reporter.ReportNote "dropdown appeared"
'Else
'	Reporter.ReportNote "dropdown didn't appear"
'	ExitTest
'End If
'
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//*[@id='mgreportcombo-1544-inputEl']").Set "Rate"
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id, 'mgreportcombo')]").Set "Rate" ' matches multiple
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div").Click ' matches more
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//*[starts-with(@class,'x-trigger-cell')]").Click 
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button' and @id='ext-gen2267']").Click ' **works but dynamic id**
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button']").Click ' match multiple
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//*[.='Rating']/../../../../div/div/div/div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td[contains(@class,'x-trigger-cell') and contains(@class, 'x-unselectable')]/div[@role='button']").Click 'located wrong to the bottom dropdown button
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']").Click
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//div/span[.='Rating']/../../../div/span/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr/td/input[starts-with(@id,'mgreportcombo')]").Set "Rate"
'msgbox Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']/../../..").getroproperty("html id")
'
'If  objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/following-sibling::*[2]").GetROProperty("innertext") = "Pending" _ 
'	And Not objPageMG.Frame("name:=Detail").WebElement("html tag:=SPAN", "innertext:=" & strLoadNumber & ": To-Do List").Exist Then
'	' 1. look for the loadnumber element, it's a <td> element
'	' 2. look for immediate preceding sibling of (1), it's also a <td> element
'	' 3. look for child of (2), it's a <div> element and it's the only child of (2)
'	' Click the plus sign to expend, or it might be a minus sign if already expanded
'	objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/preceding-sibling::*[1]/div").Click
'End If

' After finding and expanding the load
'--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
' Begin to cover the load
objPageMG.Sync
' Assign Customer Rate

' Look for Assign Customer Rate with class='mg-bubble-title and htmltext='Assign Customer Rate'
' Then look for its child and grandchild <a>/<img>
'msgbox objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Customer Rate')]/a/img").GetROProperty("html id")
objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click
'objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Customer Rate')]/a/img").Click

' ### Make sure the Rating screen pops up
strRatingXpath = "xpath:=//div[./div[contains(@id, 'frame1ML')]/div/div/div[contains(@id, 'header-body')]/div/div[contains(@id, 'header-targetEl')]/div/span[.='Rating']]"
Set objRating = objPageMG.Frame("name:=Detail").WebElement(strRatingXpath)
Do
	Wait 1
Loop Until objRating.Exist(1)

intWaitTime = 90
' dropdown Rate View
' ### combo box input part
strXpath = "//div[./div/div/div/div/div/div/div/span[.='Rating']]/following-sibling::div[2]/div/div/div[contains(@id, '-body')]/div/div/div/div/div/div[contains(@id, 'targetEl')]/table/tbody/tr/td[contains(@id, 'bodyEl')]/table/tbody/tr"
strXpathInput = "/td[contains(@id, 'inputCell')]/input[starts-with(@id, 'mgreportcombo') and contains(@id, 'inputEl')]" ' combo box input for Rating window
strXpathDrop = "/td/div[@role='button']"

i = 0
inAutotRetry = 2

Do
	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=" & strXpath & strXpathInput)
	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=(//input[starts-with(@id,'mgreportcombo')])[2]").Click ' first [1] is the combobox for Master Load Board
	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=(//input[starts-with(@id,'mgreportcombo')])[2]").Set "Rate"
	Do 
		Wait 1
	Loop Until objElement.Exist(0) ' combo box input part
	objElement.Click
	objElement.Set "Rate"

	intTimeOut = 5
	Do While Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Rate View')]").Exist(1)
		Wait 1
		intTimeOut = intTimeOut - 1
		If  intTimeOut <=0 Then
			Reporter.ReportNote "dropdown didn't appear"
			' ### combo box dropdown button
			Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=" & strXpath & strXpathDrop)
			objElement.Click
			Reporter.ReportNote "Click dropdown button"
			intTimeOut = 5
		End If
	Loop
	objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Rate View')]").Click 'working
	Reporter.ReportNote "dropdown appeared"

	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//div[@role='button'])[5]").Click 'give up locating for specific, using index number, might break due to index order changes
	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//input[starts-with(@id,'mgreportcombo')])[2]/../following-sibling::td[1]/div").Click ' This is better, relative to teh input field
	'For i = 1 to 15
	'	Set e = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']/../../../../../../../../../div[1]/*[" & i & "]")
	''	Set ec1 = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']/../../../../../../../../../div[" & i & "]/*[1]")
	''	Set ec2 = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/span[.='Rating']/../../../../../../../../../div[" & i & "]/*[2]")
	'	msgbox i & ":" & e.getroproperty("html tag") & " - " & e.getroproperty("html id") & " - " & e.GetROProperty("class")
	''	msgbox  i & ":child1 " & ec1.getroproperty("html tag") & " - " & ec1.getroproperty("html id") & " - " & ec1.GetROProperty("class")
	''	msgbox  i & ":child2 " & ec2.getroproperty("html tag") & " - " & ec2.getroproperty("html id") & " - " & ec2.GetROProperty("class")
	'Next
	'Set e = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//html/body/div[3]")
	'msgbox e.getroproperty("html tag") & " - " & e.getroproperty("html id") & " - " & e.GetROProperty("class")
	Wait 1
	j = WaitForLastLoadingComplete(intWaitTime) ' for load list population
	Reporter.ReportNote "Rating screen Rate View polulating time - time out if it equals 60: " & (intWaitTime - j)
	' ### Retry intAutoRetry Times automatically before prompt message box
	i = WaitForRadioButton(intWaitTime)
	If i <=0 Then
		If intAutoRetry <= 0 Then
		   intChoice = MsgBox("Abort to Exit Test" & vbCrLf & "Retry to wait longer" & vbCrLf & "Ignore to continue", 2, "Loading time out")
		   Select Case intChoice
				Case 3: ExitTest ' Abort
				Case 4: i = 0	' Retry
				Case 5: i = 1 ' Ignore
			End Select
		Else
			i = 0
			intAutoRetry = intAutoRetry - 1
		End If
	End If
Loop  While i <= 0

'objPageMG.Sync
'
' look for ICS DRY VAN PUBLISHED rate
'msgbox Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[.='ICS DRY VAN PUBLISHED']/preceding-sibling::td[3]/div").GetROProperty("class")
'click the radio button
If  Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[.='ICS DRY VAN PUBLISHED']").Exist Then
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[.='ICS DRY VAN PUBLISHED']/preceding-sibling::td[3]/div").Click 'working
ElseIf Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[.='ICS DRY VAN SPOT']").Exist Then
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[.='ICS DRY VAN SPOT']/preceding-sibling::td[3]/div").Click 'working
End If

If Browser("name:=Customer Rate Details").Page("title:=Customer Rate Details").WebButton("type:=button", "class:=InputText").Exist(1) Then
	Browser("name:=Customer Rate Details").Page("title:=Customer Rate Details").WebButton("type:=button", "class:=InputText").Click
End If

'find Save Selected Rate button
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save Selected Rate']").Click ' comment this to save data preparation for debuging porpose

'check if the EZ button disappears, also it's a time sync for next step
CheckEZ strCoverStep, 60