﻿Dim strURL, objWS, objPageMG
Const strBrowser = "iexplore.exe"
'Const strBrowser = "chrome.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("title:=TMS").Page("title:=TMS")

Sub ReportStrComp(strExp, strAct, strDes)
	If StrComp(strAct, strExp, 1) = 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Expected:[" & strExp & "] Actual: [" & strAct & "]"
	End If
End Sub

Sub ReportInStr(strBig, strSmall, strDes)
	If InStr(1, strBig, strSmall, 1) <> 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Small:[" & strSmall & "] Big: [" & strBig & "]"
	End If
End Sub

'--------------------------------------------------------------------------------
'Function Name: SaveDatatableToResource
'Description: The function allows run time datatable to be saved to QC Test
'             Resources module.
'             The resource name is specified in the function parameter and
'             the function will look for the resource name in QC Test Resources
'             module, then upload datatable to replace it.
'Created By: Hao Xin 
'Example: SaveDatatableToResource "test.xls"
'--------------------------------------------------------------------------------

Function SaveDatatableToResource(strResourceName)
    Dim qcConn, strTempFolder, intCount, strName
    ' Connect To QC
    Set qcConn = QCUtil.QCConnection

    ' Set Temp Folder
    strTempFolder = environment("SystemTempDir") 

    Set ResourceFactory = qcConn.QCResourceFactory
    Set ResourceList = ResourceFactory.NewList("")
    Set Resource = Nothing

	' Traverse all items to find the resource 
    For intCount = 1 To ResourceList.Count
		strName = ResourceList.Item(intCount).Name
		If UCase(strName) = UCase(strResourceName) Then
			Set Resource = ResourceList.Item(intCount)
		End If
    Next
    Set ResourceFactory = Nothing
    Set ResourceList = Nothing

    ' Assign the resource name (in QC11 it was blank)
	Resource.FileName = strResourceName
    
    ' Export Datatable to Temp Directory
    Datatable.Export strTempFolder & "\" & Resource.Filename

    Resource.Post
    ' Upload the exported datatable from temp folder to QC
    Resource.UploadResource strTempFolder, True
End Function

objPageMG.Sync
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebTable("innertext:=Add Transport").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click ' working
'objPageMG.Frame("name:=Detail").WebTable("innertext:=Add Transport").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click ' working but can be simplified below:
objPageMG.Frame("name:=Detail").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click

' BillTo Code
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=oidEnterprise").Click
objWS.SendKeys "HESAAB"
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=x-boundlist-item x-boundlist-item-over").Click

'Edit Event 1 (Pickup)
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").Image("css:=#event1 > div:nth-child(1) > a:nth-child(3) > img:nth-child(1)").Click ' working
'Browser("Google").Page("TMS").Frame("winaddMultiStopIFrame").Image("edit").Click ' OR version @@ hightlight id_;_Browser("Google").Page("TMS").Frame("winaddMultiStopIFrame").Image("edit")_;_script infofile_;_ZIP::ssf1.xml_;_

strName = "HEB"
strCity = "Houston"
'name
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sName").Set strName
'city
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sCity").Set strCity
'Click find link
Browser("title:=https:.*").Page("name:=DateRangePopup").Link("css:=#scrollingBody > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > a:nth-child(1)").Click

' Find specific value then get the row number to select the related radio button
'If  Browser("title:=Locations").Page("title:=Locations").WebRadioGroup("css:=td.DetailBodyTableRowOdd:nth-child(1) > input:nth-child(1)").Exist Then
'	Browser("title:=Locations").Page("title:=Locations").WebRadioGroup("css:=td.DetailBodyTableRowOdd:nth-child(1) > input:nth-child(1)").Click 'not working properly, always selects the first one instead
    'strHTML = Browser("title:=Locations").Page("title:=Locations").WebRadioGroup("xpath:=/html/body/form[2]/table/tbody/tr/td[.='HEB']/preceding-sibling::*[3]/input")

	'strHTML = Browser("title:=Locations").Page("title:=Locations").WebTable("name:=Location Code").GetRowWithCellText("HEB PANTRY", 4, 3) 'not exact match
	'exact match version:

	'Find first row with exact match to strNameValue (target label name to find)
	'Assign matching row number to strLabelRowNumber
	'MsgBox "Seaching Rows"
	Set objMyObj = Browser("title:=Locations").Page("title:=Locations").WebTable("name:=Location Code")
	strNameValue = strName
	intRowCount = objMyObj.RowCount
	'Msgbox intRowCount
	For i = 1 to intRowCount
		strCellData = objMyObj.GetCellData (i, 4)
		'MsgBox "Searching Row #: " &amp; i
		'MsgBox strCellData
		If strCellData = strNameValue Then
			strLabelRowNumber = i
			'MsgBox "Match Found! " &amp; vbcrlf &amp; "Row #: " &amp; strLabelRowNumber
			Exit For
		End If
	Next

'	msgbox strLabelRowNumber
	strLabelRowNumber =  strLabelRowNumber - 3 'one enpty row, one header row start from #0
'	msgbox strLabelRowNumber
	Browser("title:=Locations").Page("title:=Locations").WebRadioGroup("xpath:=/html/body/form[2]/table/tbody/tr/td[.='HEB']/preceding-sibling::*[3]/input").Select "#" & strLabelRowNumber
    'Browser("title:=Locations").Page("title:=Locations").WebElement("xpath:=/html/body/form[2]/table/tbody/tr[td[4][.='HEB'] and td[7][.='HOUSTON']]/*[1]/input").Click ' found but clicked the first one instead
'End If

'OK
Browser("title:=https:.*").Page("name:=DateRangePopup").WebButton("name:=OK").Click

'Edit Event 2
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").Image("css:=#event2 > div:nth-child(1) > a:nth-child(3) > img:nth-child(1)").Click

'Location code, type and select
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sLocationCode").Click
Set objWS = CreateObject("WScript.Shell")
objWS.SendKeys "HEHOBQ"
Browser("title:=https:.*").Page("name:=DateRangePopup").WebElement("css:=.search-loc").Click

' Verify Puckup Event field
strExpected = strName & ", "  & strCity
strActual = Browser("title:=https:.*").Page("name:=DateRangePopup").WebElement("name:=pickupEventitem1").GetROProperty("value")

'ReportStrComp strExpected, strActual, "Pickup Event field check"
ReportInStr strActual, strExpected, "Pickup Event field check"
'Browser("Browser").Page("Page").WebList("pickupEventItem1").Check CheckPoint("pickupEventItem1") @@ hightlight id_;_Browser("Browser").Page("Page").WebList("pickupEventItem1")_;_script infofile_;_ZIP::ssf3.xml_;_

' Description
strDescription = "Flour"
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sDescriptionItem1").Set strDescription
' Planned Qty
strQty = "24"
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=fPlannedQuantityItem1").Set strQty
' Palknned Wgt:
strWgt = "38447"
Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=fPlannedWeightItem1").Set strWgt

'OK
Browser("title:=https:.*").Page("name:=DateRangePopup").WebButton("name:=OK").Click

' Verify Qty, Wgt are populated on both events
strExpected = FormatNumber(strQty, 1)  & " PLT"
strActual = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemQuantity", "css:=#event1Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)").GetROProperty("innertext")
ReportStrComp strExpected, strActual, "Qty1 check"

strExpected = FormatNumber(strQty, 1)  & " PLT"
strActual = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemQuantity", "css:=#event2Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)").GetROProperty("innertext")
Call ReportStrComp(strExpected, strActual, "Qty2 check")

strExpected = FormatNumber(strWgt, 0) & " lb"
strActual = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemWeight", "css:=#event1Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3)").GetROProperty("innertext")
Call ReportStrComp(strExpected, strActual, "Wgt1 check")

strExpected = FormatNumber(strWgt, 0) & " lb"
strActual = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemWeight", "css:=#event2Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3)").GetROProperty("innertext")
Call ReportStrComp(strExpected, strActual, "Wgt2 check")

'Solicitor Code
' css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > input:nth-child(1)
' or xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[3]/td[2]/input
' or css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=SR202397500961").Set "HESAA2"

'Operational Owner
'css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2) > input:nth-child(1)
'css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
'xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[4]/td[2]/input
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=SR202744092191").Set "JISQHX3"

'Ship ID
'css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2) > input:nth-child(1)
'css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
'xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[5]/td[2]/input
' guid first 8 
Dim TypeLib
Set TypeLib = CreateObject("Scriptlet.TypeLib")
strUniqueID = Mid(TypeLib.Guid, 2, 8)
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=SR202396415561").Set strUniqueID

'Load Type
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebList("name:=SR20279792255").Select "ICS"

'Equipment
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebCheckBox("name:=sSelectedEquipment", "value:=TF:53").Set "ON"

'Special Instructions
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=sSpecialInstructions").Set "My special instructions " & strUniqueID

' Click Save button
Browser("title:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebButton("name:=Save").Click

' load is successfully created
Browser("title:=TMS").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK").Click

'Get load number
strLoadNumber = Right(Browser("title:=TMS").Page("title:=TMS").WebElement("html id:=winLoadDetail_header_hd-textEl").GetROProperty("innertext"), 10)
'msgbox strLoadNumber

' ### Add script to close the load window later ****************************************************************************************************************** something to do later
' Ways to save the load number:
' passing parameter won't work on QTP11 due to issues reported
' using data table for now

' Load the datatable from QC
'Dim fso
'Set fso = CreateObject("Scripting.FileSystemObject")

DataTable.AddSheet "LoadNumber"
XLdatafile = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls")

If Len(XLdatafile) = 0 Then
	MsgBox("The Datatable is not found in QC")
Else
	DataTable.ImportSheet XLdatafile, "LoadNumber", "LoadNumber"
End If

intRow = datatable.GetSheet("LoadNumber").GetRowCount
datatable.GetSheet("LoadNumber").SetCurrentRow(intRow + 1)

'Set TypeLib = CreateObject("Scriptlet.TypeLib")
'strUniqueID = Mid(TypeLib.Guid, 2, 8)
' Append the new load number to the datatable
DataTable.GetSheet("LoadNumber").GetParameter("LoadNumber").Value = strLoadNumber

' Save the datatablel to QC
'DataTable.Export("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls") 'This is not working
SaveDatatableToResource "mg.xls"
Reporter.ReportNote "Load " & strLoadNumber & " created at " & Date & " " & Time

' ###Close the load window to avoid later element locating problems
Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[./span[@id='winLoadDetail_header_hd-textEl']]/following-sibling::div[5]/img")
If objElement.Exist(1) Then
	objElement.Click
End If
