﻿Sub WaitForLoadPopulating(intTimeOut) ' by checking if a plus sign button is showing up
   Dim i
	i = intTimeOut
   Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[@id='winportletEZClickLoadMasterLoadBoard-body']/div/div/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/div").Exist) and i <= 0
	   Wait 1
	   i = i - 1
   Loop
End Sub

Sub CollapseAllLoads
   	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])[1]/div"
	strElement = "xpath:=(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])[1]/div"
    Set objPlus = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
    Do While objPlus.Exist(1)
		 objPlus.Click
		 Set objPlus = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
    Loop
End Sub

Function OnlyOneLoadExpanded
   	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])[1]/div"
	strElement = "xpath:=//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))]/div"
    Set objPlus = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
    If objPlus.Exist(1) Then
		OnlyOneLoadExpanded = True
	Else
		OnlyOneLoadExpanded = False
    End If
End Function
' Get Load Number from QC datatable
'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
' Consider to take searching load number and click the plus sign to expend to a seperate component later
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")

'Dim fso
'Set fso = CreateObject("Scripting.FileSystemObject")
If Parameter("strLoadNumber") = "" Then
	DataTable.AddSheet "LoadNumber"
	XLdatafile = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls")
	
	If Len(XLdatafile) = 0 Then
		MsgBox("The Datatable is not found in QC")
	Else
		DataTable.ImportSheet XLdatafile, "LoadNumber", "LoadNumber"
	End If
	
	
	intRow = datatable.GetSheet("LoadNumber").GetRowCount
	datatable.GetSheet("LoadNumber").SetCurrentRow(intRow)
	strLoadNumber = datatable.GetSheet("LoadNumber").GetParameter("LoadNumber").Value
Else
	strLoadNumber = Parameter("strLoadNumber")
End If

'-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
'  ### look for the load on Master Load Board
'  ### first set Created By Me
objPageMG.Sync
intWaitSeconds = 0
Do
	objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Set "" 'works
	wait intWaitSeconds
	objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Click
	wait intWaitSeconds
	objWS.SendKeys "Created"
	wait intWaitSeconds
	Reporter.ReportEvent micDone, "intWaitSeconds", intWaitSeconds
	intWaitSeconds = intWaitSeconds + 1
Loop Until objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Exist(0) Or intWaitSeconds > 5

If intWaitSeconds <= 5 Then
	objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").WaitProperty "visible", "True"
	objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Click 'working
	Reporter.ReportNote "dropdown appeared"
Else
	Reporter.ReportNote "dropdown didn't appear"
	ExitTest
End If

'Wait 5 '' for load list population do something similar to waitforloading
WaitForLoadPopulating(60)

' The following commented statements are those I wanted to try to click the dropdown button if the dropdownlist doesn't appear after entering "Created"
' I have spent 2 days (more than 18 hours) plus some of Roshan's time on it but still in vain because the dynamic element attributes

'objPageMG.Frame("name:=Detail").WebEdit("name:=mgcombo.*").Set "" 'not working for dynamic name, match more than one
'objPageMG.Frame("name:=Detail").WebEdit("name:=mgcombo.*").Click
'objPageMG.Frame("name:=Detail").WebEdit("name:=mgcombo1099").Set "" 'work for now but will not work if property value changes
'objPageMG.Frame("name:=Detail").WebEdit("xpath:=//*[@name='mgcombo1099']").Click ' work for now but won't if property value changes
'objPageMG.Frame("name:=Detail").WebEdit("xpath:=/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/input").Click
'wait 1
'objWS.SendKeys "  Created By Me"
'wait 1
'' if the dropdown is not showing
'If Not objPageMG.Frame("name:=Detail").WebElement("innertext:=  Created By Me").Exist Then
''	objPageMG.Frame("name:=Detail").WebElement("xpath:=//*[@name='mgcombo1099']/../following-sibling::*/div").Click
'	objPageMG.Frame("name:=Detail").WebElement("xpath:=//*[@id='ext-gen1377']").Click
'
'	wait 2
'End If
'objPageMG.Frame("name:=Detail").WebElement("innertext:=   Created By Me").Click

'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div/table/tbody/tr/td[2]/table/tbody/tr/td[1]/inputt").Set ""
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("html id:=mgreportcombo.*").Click

'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//*[@id='mgreportcombo.*']").Click

'Browser("name:=Google").Page("title:=Google").WebEdit("xpath:=//*[@name='q']").Set "//*[@name='q']" 'works
'Browser("name:=Google").Page("title:=Google").WebEdit("xpath:=//*[contains(@name,'q')]").Set "xpath:=//*[contains(@name,'q')]" 'works
'Browser("name:=Google").Page("title:=Google").WebEdit("xpath:=//*[starts-with(@name,'q')]").Set "xpath:=//*[starts-with(@name,'q')]" 'works

'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("html id:=mgreportcombo.*").Set "" 'works
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("html id:=mgreportcombo.*").Click 'works
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//*[starts-with(@id,'mgreportcombo')]").Set "asdf" 'works
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//*[starts-with(@id,'mgreportcombo')]").Set "" 'works

'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//*[starts-with(@id,'mgreportcombo')]").Click 'works

'objWS.SendKeys "  Created" 'works

' if the dropdown not working, try to click the dropdown button. this was never successful till 9/11/2014
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Set "Created By Me" 'works
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("html id:=ext-gen1381").Click 'testing
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]/following-sibling::*[1]").Click 'testing
'wait 5
'Browser("TMS").Page("TMS").Frame("Detail").WebEdit("xpath:=/html/body/div[1]/div/div/div[1]/div[2]/div[2]/div/div/div/div[2]/div/div/table/tbody/tr/td[2]/table/tbody/tr/td[2]/div").click


'------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
' Look for the load
'objPageMG.Frame("name:=Detail").WebElement("innertext:=" & strLoadNumber).Click

' First condition is to make sure Status is pending
' The status is the 2nd following sibling
' Second condition is to make sure it's not expanded yet, if it's already expanded, then do not need to click the plus sign any more
Do 
	CollapseAllLoads
	If  objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/following-sibling::*[2]").GetROProperty("innertext") = "Pending" _ 
		And Not objPageMG.Frame("name:=Detail").WebElement("html tag:=SPAN", "innertext:=" & strLoadNumber & ": To-Do List").Exist(0) Then
		' 1. look for the loadnumber element, it's a <td> element
		' 2. look for immediate preceding sibling of (1), it's also a <td> element
		' 3. look for child of (2), it's a <div> element and it's the only child of (2)
		' Click the plus sign to expend, or it might be a minus sign if already expanded
		objPageMG.Frame("name:=Detail").WebElement("xpath:=//td[.='" & strLoadNumber & "']/preceding-sibling::*[1]/div").Click
		Reporter.ReportEvent micPass, "load is found", "Pass"
	Else
		Reporter.ReportEvent micFail, "load is NOT found", "Fail"
	End If
Loop Until OnlyOneLoadExpanded

'--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
' Next to cover the load
' off of the screen, bring it back by clicking "Master Load Board twice
Set objElement = objPageMG.Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Master Load Board']")
objElement.Click
Wait 1
objElement.Click

