﻿Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")

' ### Load functionLibrary from QC
Sub LoadFLfromQC
	strFunctionLibraryMG = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mgfl")
	LoadFunctionLibrary strFunctionLibraryMG
End Sub

Sub ReportStrComp(strExp, strAct, strDes)
	If StrComp(strAct, strExp, 1) = 0Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Expected: [" & strExp & "] Actual: [" & strAct & "]"
	End If
End Sub

Sub ResizeWindow(strBrowser,intWidth, intHeight)
	Dim hwnd
	hwnd = Browser(strBrowser).Object.HWND
	Window("hwnd:=" & hwnd).Resize intWidth, intHeight
End Sub

' ### first WaitForLoading version, no options, just continue
Sub WaitForLoading1(intTimeOut) ' by check if radio buttons are present
   Dim i
	i = intTimeOut
   Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//div[contains(@class, 'mg-grid-row-radio')])[1]").Exist) and i <= 0
	   Wait 1
	   i = i - 1
   Loop
End Sub

Sub WaitForLoadPopulating(intTimeOut) ' by check if a plus sign button present on Master Load Board
   Dim i
	i = intTimeOut
   Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[@id='winportletEZClickLoadMasterLoadBoard-body']/div/div/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/div").Exist(0)) and i <= 0
	   Wait 1
	   i = i - 1
   Loop
End Sub

Sub WaitForLoadingComplete(intTimeOut)
	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "//div[./div/div[normalize-space(.)='Loading...']]"
	strDisplay = ""
	Do While strDisplay = ""
			intLoading = CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath)
			If intLoading = 0 Then
				Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).Webelement("xpath:=" & strXpath)
				strDisplay = objElement.Object.style.display
'				Reporter.ReportNote strDisplay
			End If
			intNone = 0
			If intLoading > 0 Then
				For i = 1 to intLoading
					Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).Webelement("xpath:=(" & strXpath & ")[" & i & "]")
					If  objElement.Object.style.display = "none" Then
						intNone = intNone + 1
					End If
'					strOuter = objElement.GetROProperty("outerhtml")
'					Reporter.ReportNote strDisplay & strOuter
				Next
				If intNone = intLoading Then
					strDisplay = "none"
				End If
			End If
			Reporter.ReportNote Time
		'	wait 1
	Loop
	'msgbox "loading complete"
End Sub

' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
' ### Wait until an element exist, can't use "Loading..." label becuase its "visible" is always True
Function WaitForRadioButton(intTimeOut) ' by check if radio buttons are present
   Dim i
	i = intTimeOut
   Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=(//div[contains(@class, 'mg-grid-row-radio')])[1]").Exist(0)) and i > 0
	   Wait 1
	   i = i - 1
   Loop
   WaitForRadioButton = i
'   Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[starts-with(.='ICS DRY VAN PUBLISHED']").Exist 
End Function

' There are multiple Loading... elements, currently the last() one is for the Rating window and no certainty to idnetify by other dynamic attributes
'  When loading is done, the Loading... element's style changes from not containing "display: none"  to containing it. xpath:=//*[@style] doesn't work
' Looking for the last Loading... element Object.style.display value exists and equals to 'none'
Function WaitForLastLoadingComplete(intTimeOut)
	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "//div[./div/div[normalize-space(.)='Loading...']]"

	' ### Make sure the Rating screen pops up
	strRatingXpath = "xpath:=//div[./div[contains(@id, 'frame1ML')]/div/div/div[contains(@id, 'header-body')]/div/div[contains(@id, 'header-targetEl')]/div/span[.='Rating']]"
	Set objRating = objPageMG.Frame("name:=Detail").WebElement(strRatingXpath)
	Do
		Wait 1
	Loop Until objRating.Exist(1)

	' ### Then check if Loading... disappears
	strElement  = "xpath:=" & strXpath
	strElement1 = "xpath:=(" & strXpath & ")[1]"
	strDisplay = ""
	i = intTimeOut
	Do While strDisplay = "" and i > 0
		Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
		Set objElement1 = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement1)
		If  objElement.Exist(0) Then
			strDisplay = objElement.Object.style.display
		ElseIf  objElement1.Exist(0) Then
			Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).Webelement("xpath:=(" & strXpath & ")[last()]")
			strDisplay = objElement.Object.style.display
		Else
			strDisplay = "none"
		End If
		i = i - 1
	Loop
	WaitForLastLoadingComplete = i
'	msgbox "loading complete"
End Function

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub

' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
' ### Utility Function
' ### To verify wheather xpath expression is valid
' ### Pass strFrame empty string if no frame layer for the element
Function CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath)
	Dim objElement, objElement1, objElementi, i
	strElement  = "xpath:=" & strXpath
	strElement1 = "xpath:=(" & strXpath & ")[1]"
'	msgbox strElement & " " & strElement1
	If Len(strFrame) = 0 Then
		Set objElement = Browser(strBrowser).Page(strPage).WebElement(strElement)
		Set objElement1 = Browser(strBrowser).Page(strPage).WebElement(strElement1)
	Else
		Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
		Set objElement1 = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement1)
	End If
	CheckWebElementXpath = -1
	If objElement.Exist(1) Then
		strNote = "one match tag:" & objElement.GetROProperty("html tag") & " id:" & objElement.GetROProperty("html id") & " name:" & objElement.GetROProperty("name") & " class:" & objElement.GetROProperty("class") & " text:" & objElement.GetROProperty("innertext") & " value:" & objElement.GetROProperty("value") & " visible:" & objElement.GetROProperty("visible")  & " style:" & objElement.Object.style.cssText
		Reporter.ReportNote strNote
		'msgbox strNote
		CheckWebElementXpath = 0
	ElseIf objElement1.Exist(1) Then
		i = 1
		strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
		If Len(strFrame) = 0 Then
			Set objElementi = Browser(strBrowser).Page(strPage).WebElement(strElementi)
		Else
			Set objElementi = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElementi)
		End If
		Do While objElementi.Exist(1)
			strNote = "multi match tag:" & objElementi.GetROProperty("html tag") & " id:" & objElementi.GetROProperty("html id") & " name:" & objElementi.GetROProperty("name") & " class" & objElementi.GetROProperty("class") & " text:" & objElementi.GetROProperty("innertext") & " value:" & objElementi.GetROProperty("value") & " visible:" & objElementi.GetROProperty("visible")  & " style:" & objElementi.Object.style.cssText
			Reporter.ReportNote strNote
			i = i + 1
			strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
			If Len(strFrame) = 0 Then
				Set objElementi = Browser(strBrowser).Page(strPage).WebElement(strElementi)
			Else
				Set objElementi = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElementi)
			End If
		Loop
		CheckWebElementXpath = i - 1
	Else
		CheckWebElementXpath = -1
		Reporter.ReportNote  "not found"
		'msgbox "not found"
	End If
'	wait 1
	Reporter.ReportNote "Total matches: " & CheckWebElementXpath
End Function

' ### WebEdit version of the above
Function CheckWebEditXpath(strBrowser, strPage, strFrame, strXpath)
	Dim objElement, objElement1, objElementi, i
	strElement  = "xpath:=" & strXpath
	strElement1 = "xpath:=(" & strXpath & ")[1]"
'	msgbox strElement & " " & strElement1
	Set objElement = Browser(strBrowser).Page(strPage).Frame(strFrame).WebEdit(strElement)
	Set objElement1 = Browser(strBrowser).Page(strPage).Frame(strFrame).WebEdit(strElement1)
	CheckWebEditXpath = -1
	If objElement.Exist(1) Then
		Reporter.ReportNote "one match " & objElement.GetROProperty("html tag") & " " & objElement.GetROProperty("html id") & " " & objElement.GetROProperty("innertext") & " " & objElement.GetROProperty("visible")
		CheckWebEditXpath = 0
	ElseIf objElement1.Exist(1) Then
		i = 1
		strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
		Set objElementi = Browser(strBrowser).Page(strPage).Frame(strFrame).WebEdit(strElementi)
		Do While objElementi.Exist(1)
			Reporter.ReportNote "multi match " & objElementi.GetROProperty("html tag") & " " & objElementi.GetROProperty("html id") & " " & objElementi.GetROProperty("innertext") & " " & objElementi.GetROProperty("visible")
			i = i + 1
			strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
			Set objElementi = Browser(strBrowser).Page(strPage).Frame(strFrame).WebEditt(strElementi)
		Loop
		CheckWebEditXpath = i - 1
	Else
		CheckWebEditXpath = -1
		Reporter.ReportNote  "not found"
	End If
End Function

' ### Dialog version of the above
Function CheckDialogElementXpath(strBrowser, strDialog, strType, strXpath)
	Dim objElement, objElement1, objElementi, i
	strElement  = "regexpwndtitle:=OK"
	strElement1 = "text:=OK"
'	msgbox strElement & " " & strElement1
	Select Case strType
	Case "WinButton"
		Set objElement = Browser(strBrowser).Dialog(strDialog).WinButton(strElement)
		Set objElement1 = Browser(strBrowser).Dialog(strDialog).WinButton(strElement1)
	Case "WebElement"
		Set objElement = Browser(strBrowser).Dialog(strDialog).WebElement(strElement)
		Set objElement1 = Browser(strBrowser).Dialog(strDialog).WebElement(strElement1)
	End Select
	CheckDialogElementXpath = -1
	If objElement.Exist(1) Then
		strNote = "one match tag:" & objElement.GetROProperty("html tag") & " id:" & objElement.GetROProperty("html id") & " name:" & objElement.GetROProperty("name") & " class:" & objElement.GetROProperty("class") & " text:" & objElement.GetROProperty("innertext") & " value:" & objElement.GetROProperty("value") & " " & objElement.GetROProperty("visible")
		Reporter.ReportNote strNote
		msgbox strNote
		CheckDialogElementXpath = 0
	ElseIf objElement1.Exist(1) Then
		i = 1
		strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
		Select Case strType
		Case "WinButton"
			Set objElementi = Browser(strBrowser).Dialog(strDialog).WinButton(strElement)
		Case "WebElement"
			Set objElementi = Browser(strBrowser).Dialog(strDialog).WebElement(strElement)
		End Select
		Do While objElementi.Exist(1)
			strNote = "multi match tag:" & objElementi.GetROProperty("html tag") & " id:" & objElementi.GetROProperty("html id") & " name:" & objElement.GetROProperty("name") & " class" & objElement.GetROProperty("class") & " text:" & objElementi.GetROProperty("innertext") & " value:" & objElementi.GetROProperty("value") & objElementi.GetROProperty("visible")
			Reporter.ReportNote strNote
			'msgbox strNote
			i = i + 1
			strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
			Set objElementi = Browser(strBrowser).Dialog(strDialog).WebElement(strElementi)
		Loop
		CheckDialogElementXpath = i - 1
	Else
		CheckDialogElementXpath = -1
		Reporter.ReportNote  "not found"
	End If
'	wait 1
End Function

Sub CheckElement
	On Error Resume Next
	strBrowser = "name:=TMS"
	''strBrowser = "title:=https:.*"
	'strBrowser = "title:=https://jbhunt2.mercurygate.net/MercuryGate/transport/multiStopEditEvent.jsp?norefresh=&eventNumber=2"
	strPage = "title:=TMS"
	'strPage = "name:=DateRangePopup"
	strFrame = ""
	'strFrame = "name:=Detail"
	'strFrame = "title:=List References"
    'strFrame = "html id:=referencesWin"
    'strFrame = "name:=winExtDispatch.*IFrame"
	'strFrame = "name:=winExtTender.*IFrame"
	strFrame = "name:=winaddMultiStopIFrame"
	'strFrame = "name:=winExtmanageSpotQuoteIFrame"
	'strXPath = "xpath:=//td/div[@role='button']" ' is this working?
	'strSearchLabel = "Save"
	'strXpath = "//td[following-sibling::td[1][.='A ONE EXPRESS'] and following-sibling::td[5][.='TRUCK DRY VAN']]/div/img"
	'strXpath = "//div[contains(@class, 'mg-grid-row-radio')]"
	'strXpath = "//div[./div/div/div/div/div/div/div/div/span[.='Master Load Board']]/div[12]"
	' this is the first plus sign on the list
	' using this to check if the loads are populated
	'=================================================================================9/26/2014
	'strXpath = "//div[@id='winportletEZClickLoadMasterLoadBoard-body']/div/div/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/div" ' This is the plus sign of first load on Mater Load Board
	'strXpath = "//table[./tbody/tr/td/table/tbody/tr/td/input[@name='priceSheetChargeQuantity3']]/following-sibling::table[1]/tbody/tr/td/div[2]" ' This is Fuel Surcharge amount
	'strXpath = "//table[./tbody/tr/td/div[.='Total:']]/following-sibling::table[1]/tbody/tr/td[2]/div" ' This is Total amount on Edit Quote Charges screen
	'strXpath = "//td[./input[@name='priceSheetChargeRateQualifier3']]/following-sibling::td[1]/div"This is the dropdown button of the Fuel Surcharge qualifier on Edit Quote Charges screen
	'strXpath = "//td[./label[.='Distance:']]/following-sibling::td[1]/table/tbody/tr/td/input" 'This is the distance text box on Edit Quote Charges screen
	'strXpath = "//input[@name='sEmailTo']" 'EmailTo field
	'strXpath = "//input[@value='Send']" 'Send button. it works like this
	'strXpath = "//td[@class='sectionTitle']" 'it works!!!
	'strXpath = "//textarea[@name='sTenderComments']"
	'strXpath = "//span/span/span[.='Master Load Board']" ' the button
	' ### ----------------------------------------------------------------not necessary: strXpath = "//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), '" & strCoverStep & "')]/a/img"
     ' ### ---------------------------------------------------------------this is working:  strXpath = "//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'][1]/a/img"
	'strXpath = "//div[@class='mg-bubble-title']"
	 'strXpath = "//div[.='Cover ']"'found
	 'strXpath = "//div[@class='mg-bubble-title' and normalize-space(.)='Doc Pending BOL']/a/img"
	 'strXpath = "//input[@name='arrivalETADate']"
	 'strXpath = "//span/span/span[.='Save']/following-sibling::*[1]"
	 'strXpath = "//div[./span[@id='winLoadDetail_header_hd-textEl']]/following-sibling::div[5]/img" ' ### close button of the newly created load window
	 'strSearchLabel = "Carrier Name (Begins):"
	 'strXpath="//div[./div/div/div/div/div/table/tbody/tr/td/label[.=normalize-space('" & strSearchLabel & "')]]/following-sibling::div[2]/div/div/a/table/tbody/tr/td/span/span/span[.='OK']" 'Filter popup OK button
	 'strXpath = "//div[.//span[.='Add Carrier From CARMA Carrier List']]/following-sibling::div[4]//span/span/span[.='OK']" ' Ok button for Manage Quotes window
	 'strXpath = "//div[./span[normalize-space(.)='Manage Quotes']]/following-sibling::div[5]/img"
	strCoverStep = "Set Arrival/Departure"
	'strCoverStep = "Verify Data"
	'strLinkofVerifyData = "/following-sibling::div[1]/div/a"
	'strXpath = "(//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'])[1]/a/img"
	'strXpath = "//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']"
	 'strXpath = "//span/span/span[.='Save']/following-sibling::*[1]"
	 'strXpath = "//div[@class='x-grid-row-expander']"
	 'strXpath = "//span/span[.='Details']"
	 'strXpath = "(//td[normalize-space(.)='Operational Owner'])[1]" ' there are 2, the second one is without edit icon
	 'strXpath = "(//td[normalize-space(.)='Operational Owner'])[1]/preceding-sibling::td[3]/span/a[2]/img"
     'strXpath = "//td[normalize-space(.)='Solicitor Code']"
	 'strFrame = "html id:=referencesWin"
	'strXpath = "(//td[normalize-space(.)='Operational Owner'])[1]/preceding-sibling::td[1]/a"
	'strXpath = "//td[./label[normalize-space(.)='Status:']]/following-sibling::td[1]/table/tbody/tr/td/input" ' Verify Data link->Edit Activity: Verify Data->Status value field
	'strXpath = "//div[./span[normalize-space(.)='Edit Activity: Verify Data']]/following-sibling::div[5]/img"
	'strXpath = "//div[./span[.='1100003139' and contains(@id, 'header')]]/following-sibling::div[5]/img" ' Close button of Details window, replace the load number with correct one
	'strXpath = "(//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'])[1]/a/img"
	'strXpath = "//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'][1]"
	'strXpath = "//span/span/span[.='Save']/following-sibling::*[1]" ' Save button on Set Arrival/Departure window
	'strXpath = "//td[contains(@class, 'saj-special x-grid-row-collapsed')]" 'load's plus signs when collapsed
	'strXpath = "//span[id='winportletEZClickLoadMasterLoadBoard_header_hd-textEl']"
	'strXpath = "//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))]" 'works for all expanded!!! Hooray!!!
	'strXpath = "(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])/div" 'works for all expanded!!! Hooray!!!
'	strXpath = "//*[not(@disabled)]"
'	msgbox CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath)
	'strXpath = "//label[normalize-space(.)='Reason Code:']"

	'strXpath = "//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special')]" 
	'strXpath = "//td[ends-with(@class, 'x-grid-row-collapsed')]" 
	'strXpath = "//*[starts-with(@class, 'actualcell')]" 
' research for plus sign minus sign tomorrow - 5:pm 10/7/14
	'strXpath = "//td[normalize-space(@class)='actualcell sajcell-row-lines saj-special x-grid-row-collapsed']" ' not works
	'strXpath = "//td[normalize-space(@class)='actualcell sajcell-row-lines saj-special']" ' not works
	'"xpath:=(//span/span/span[.='OK'])[last()]"
	'strType = "WinButton"
	'strDialog = "regexpwndtitle:=Message from webpage"
'	strXpath = "//input[starts-with(@id,'mgreportcombo')]"
'	strXpath = "(//input[starts-with(@id,'mgreportcombo')])[2]/../following-sibling::td[1]/div[@role='button']"
'	strXpath = "//div[./div[contains(@id, 'frame1ML')]/div/div/div[contains(@id, 'header-body')]/div/div[contains(@id, 'header-targetEl')]/div/span[.='Rating']]/following-sibling::div[2]/div/div/div[2]/div/div/div/div/div/div[contains(@id, 'targetEl')]/table/tbody/tr/td"
'	strXpath = "//div[./div/div/div/div/div/div/div/span[.='Rating']]/following-sibling::div[2]/div/div/div[contains(@id, '-body')]/div/div/div/div/div/div[contains(@id, 'targetEl')]/table/tbody/tr/td[contains(@id, 'bodyEl')]/table/tbody/tr/td/div[@role='button']" ' combo box dropdown
'	strXpath = "//div[./div/div[normalize-space(.)='Loading...']]"
	'strXpath = "//div[starts-with(text(), 'OPHA5-SHP')]" ' works, not starts-with(., 'asdfafd') or starts-with(.='asdfas')
	'strXpath = "//*[@name='sLocationCode']"
'	msgbox CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath)
	'strXpath = "//input"
	strXpath = "//td[.='Operational Owner']/following-sibling::td[1]/input"
	strXpath = "//td[.='LoadType']/following-sibling::td[1]/select"
	msgbox CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath)
	'msgbox CheckWebElementXpath("name:=Choose Contact", "title:=Choose Contact", "", strXpath)
	'msgbox CheckWebElementXpath(strBrowser, strDialog, strType, strXpath)
	'msgbox Browser(strBrowser).GetROProperty("creationtime")
End Sub

Sub CommentedBlock ' check all browsers (supported browsers only)
	Set b=Description.Create
	b("micclass").value="Browser"
	Set obj=Desktop.ChildObjects(b)
	msgbox obj.count
	For i=0 to obj.count-1
	c=obj(i).getroproperty("name")
	d=obj(i).getroproperty("creationtime")
	msgbox(c)
	msgbox(d)
	'obj(i).Close
	Next
End Sub

Sub CommentedBlock2
'   msgbox date
'   For i = 0 to 4
'msgbox FormatDateTime(date, i)
'   Next
   strDate = Date
   strDate = Right("0" & DatePart("m",strDate), 2) _ 
        & "/" & Right("0" & DatePart("d",strDate), 2) _
		& "/" & DatePart("yyyy",strDate)
		msgbox strDate

		' ### get all attributte of an object
		Set Obj = objElementi.Object
		For i = 0 to Obj.Attributes.Length-1
			Reporter.ReportNote Obj.Attributes(i).Name & "---" & Obj.Attributes(i).Value
		Next

End Sub

Function uniqueString(ByVal intLength)
   If Not IsNumeric(intLength) Then
	   intLength = 8
   End If
   If intLength < 1 or intLength > 32 Then
	   intLength = 32
   End If
	Set TypeLib = CreateObject("Scriptlet.TypeLib")
	strGuid = TypeLib.Guid
	strGuid = Replace(strGuid, "-", "")
	uniqueString = Mid(strGuid, 34-intLength, intLength)
End Function

'wait 1

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote "Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub

Sub VerifyData
	'  ### first set Created By Me
	objPageMG.Sync
	If Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[@id='winportletEZClickLoadMasterLoadBoard-body']/div/div/div/div[4]/div[2]/table/tbody/tr[1]/td[1]/div").Exist(0) Then ' first plus sign on Master Load Board
		intWaitSeconds = 0
		Do
			objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Set "" 'works
			wait intWaitSeconds
			objPageMG.Frame("name:=Detail").WebEdit("xpath:=//input[starts-with(@id,'mgreportcombo')]").Click
			wait intWaitSeconds
			objWS.SendKeys "Created"
			wait intWaitSeconds
			Reporter.ReportEvent micDone, "intWaitSeconds", intWaitSeconds
			intWaitSeconds = intWaitSeconds + 1
		Loop Until objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Exist(0) Or intWaitSeconds > 5
		
		If intWaitSeconds <= 5 Then
			objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").WaitProperty "visible", "True"
			objPageMG.Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Created By Me')]").Click 'working
			Reporter.ReportNote "dropdown appeared"
		Else
			Reporter.ReportNote "dropdown didn't appear"
			ExitTest
		End If
	End If
	
	'Wait 5 
	WaitForLoadPopulating(60)

	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strCoverStep = "Verify Data"
	strXpathVerifyData = "//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']"
	strXpathVerifyDataLink = strXpathVerifyData & "/following-sibling::div[1]/div/a"
	' ### loop over all pending load
	 strXpath = "//div[@class='x-grid-row-expander']/../following-sibling::td[3][.='Pending']/preceding-sibling::*[3]/div" ' ### plus(or minus) sign of all Pending loads - multiple match
	 strXpathLoadNumber = "//div[@class='x-grid-row-expander']/../following-sibling::td[3][.='Pending']/preceding-sibling::*[2]" ' ### Load Number of Pending loads - multiple match
	 intPlusSigns = CheckWebElementXpath(strBrowser, strPage, strFrame, strXpath) ' ### number of pending loads on the screen
	If intPlusSigns > 0 Then
		For i = 1 to intPlusSigns
			strElementi = "xpath:=(" & strXpath & ")[" & i & "]"
			strElementLoadNumberi = "xpath:=(" & strXpathLoadNumber &  ")[" & i & "]"
			Set objPlusSign = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElementi)
			' ### get the load number
			strLoadNumber = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElementLoadNumberi).GetROProperty("innertext")
			objPlusSign.Click ' ### expand the plus sign
			blnSkip = False
			Do While  (CheckWebElementXpath(strBrowser, strPage, strFrame, strXpathVerifyData) = 0) And Not blnSkip ' ### Verify Data EZ button appears
				' ###check what the error is by clicking the date link of the Veryfy Data EZ Click button
				Set objLink = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement("xpath:=" & strXpathVerifyDataLink)
				objLink.Click
				strError = Browser(strBrowser).Page(strPage).Frame(strFrame).WebEdit("xpath:=//td[./label[normalize-space(.)='Status:']]/following-sibling::td[1]/table/tbody/tr/td/input").GetROProperty("value")
				' ### Close Edit Activity: Verify Data window by clicking Close button
				Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement("xpath:=//div[./span[normalize-space(.)='Edit Activity: Verify Data']]/following-sibling::div[5]/img").Click
				Reporter.ReportNote "Processed Verify Data for load " & strLoadNumber & " with error " & strError
				If Not (strError = "Invalid Operational Owner" Or strError = "Incorrect Solicitor Code") Then
					blnSkip = True
				End If
				'===========================================================
				' ### click the Detail button
				Set objDetailsButton = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement("xpath:=//span/span[.='Details']")
				objDetailsButton.Click

				' ### Modify Operational Owner to a valid value if not
				Set objEditItem = Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Operational Owner'])[1]")
				strCorrectValue = "jisqhx3"
				' ### wait for loading
				Do While Not objEditItem.Exist(1)
					Wait 1
				Loop
				' ###check if the Operational Owner is valid
				'If UCase(strEditText) <> UCase("jisqhx3") Then
				If UCase(strError) = UCase("Invalid Operational Owner") Then
					strEditText = Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Operational Owner'])[1]/preceding-sibling::td[1]").GetROProperty("text") ' ### the original value
					Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Operational Owner'])[1]/preceding-sibling::td[1]/a").Click
					Do While Not Browser("title:=Edit Reference").Page("title:=Edit Reference").WebEdit("default value:=" & strEditText).Exist(1)
						Wait 1
					Loop
					'Browser("name:=Edit Reference").Page("name:=Edit Reference").Link("default value:=" & strEditText).Click
					Browser("title:=Edit Reference").Page("title:=Edit Reference").WebEdit("default value:=" & strEditText).Set strCorrectValue
					Browser("title:=Edit Reference").Page("title:=Edit Reference").WebButton("name:=Save").Click
					Reporter.ReportNote "Modified Operational Owner for load" & strLoadNumber & " from " & strEditText & " to " & strCorrectValue
					Window("ispopupwindow:=True", "text:=Edit Reference - Windows Internet Explorer").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK").Click
				End If

				' ### modify Solicitor Code to a valid value if not
				Set objEditItem = Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Solicitor Code'])[1]")
				strCorrectValue = "HESAA2-SOL"
'				Do While Not objEditItem.Exist(1)
'					Wait 1
'				Loop
				' ###check if the Solicitor Code is valid
				If UCase(strError) = UCase("Incorrect Solicitor Code") Then
				'If UCase(strEditText) <> UCase("HESAA2-SOL") Then
					strEditText = Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Solicitor Code'])[1]/preceding-sibling::td[1]").GetROProperty("text") ' ### the oringinal value
					Browser(strBrowser).Page(strPage).Frame("html id:=referencesWin").WebElement("xpath:=(//td[normalize-space(.)='Solicitor Code'])[1]/preceding-sibling::td[1]/a").Click ' ### debug to here on 10/6/14 6pm, looks good. continue tomorrow
					Do While Not Browser("title:=Edit Reference").Page("title:=Edit Reference").WebEdit("default value:=" & strEditText).Exist(1)
						Wait 1
					Loop
					Browser("title:=Edit Reference").Page("title:=Edit Reference").WebEdit("default value:=" & strEditText).Set strCorrectValue
					Browser("title:=Edit Reference").Page("title:=Edit Reference").WebButton("name:=Save").Click
					Reporter.ReportNote "Modified Solicitor Code for load " & strLoadNumber & " from " & strEditText & " to " & strCorrectValue
					Window("ispopupwindow:=True", "text:=Edit Reference.*").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK").Click
				End If
				' ### Close Details window
				Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement("xpath:=//div[./span[.='" & strLoadNumber & "' and contains(@id, 'header')]]/following-sibling::div[5]/img").Click
				Wait 1
			Loop
			objPlusSign.Click
		Next
	End If
	'msgbox intPlusSigns
End Sub

Sub CollapseAllLoads
   	strBrowser = "name:=TMS"
	strPage = "title:=TMS"
	strFrame = "name:=Detail"
	strXpath = "(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])[1]/div"
	strElement = "xpath:=(//td[contains(normalize-space(@class), 'actualcell sajcell-row-lines saj-special') and not(contains(@class, 'x-grid-row-collapsed'))])[1]/div"
    Set objPlus = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
    Do While objPlus.Exist(1)
		 objPlus.Click
		 Set objPlus = Browser(strBrowser).Page(strPage).Frame(strFrame).WebElement(strElement)
    Loop
End Sub

'VerifyData
'strCoverStep = "Assign Customer Rate"
'objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click
CheckElement
'WaitForLastLoadingComplete(120)
'wait 1
'CommentedBlock2

'CollapseAllLoads

Function SaveScript(strQCPath, strLocalPath)
   'Set qcConn = QCUtil.QCConnection
   Set qtApp = CreateObject("QuickTest.Application")
    qtApp.Launch
	qtApp.Visible = True
	If Not qtApp.TDConnection.IsConnected Then
	
		' Make changes in a test on Quality Center with version control
		qtApp.TDConnection.Connect "hpqcenter","","DailyTesting","jisqhx3","Password",False
		
		'QC URL = QC Server path
		'DOMAIN Name = Domain name that contains QC project
		'Project Name =Project Name in QC you want to connect to
		'Username = Username to connect to Project
		'Password = Password to connect to project
		'False or True = Whether ‘password is entered in encrypted or normal. 
		'Value is True for encrypted and FALSE for normal
		
		'Example : qtApp.TDConnection.Connect
		'"http://200.168.1.1:8080/qcbin","Default","proj1","qtpworld","qtp",false
	
	End If


	file = PathFinder.Locate("[QualityCenter] Components\MIB\MG\trials")
	Set myComp = qtApp.BusinessComponent
	Set myAct = myComp.Actions.Item(1)
	strScript = myAct.GetScript
	'Do whatever with the script
	'Do whatever with the script
End Function
'SaveScript "aaa", "bbb"

'[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls


