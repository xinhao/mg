﻿'
'Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
intTimeOut = 20
strCoverStep = "Set Arrival/Departure"
'Sub Commented

' ### Look for Set Arrival/Departure with class='mg-bubble-title and htmltext=strCoverStep
' ### Then look for its child and grandchild <a>/<img>
' ### since there are multiple stops, add()[1]

Do While objPageMG.Frame("name:=Detail").WebElement("xpath:=(//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'])[1]").Exist(1)
	objPageMG.Frame("name:=Detail").WebElement("xpath:=(//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "'])[1]/a/img").Click
	'/****
	'wait 1
	
	'End Sub
	' Wait until the window opens by check the Save button
	i = intTimeOut
	Do While (Not objPageMG.Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save']/following-sibling::*[1]").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote  strCoverStep & " - Loop times for the window to appear: " & (intTimeOut - i)
		
	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("name:=arrivalETADate")
	objElement.WaitProperty "visible", True
	strDate = Date
	' ### add leading zero to month, date if windows short date format doesn't have
	' ### MG won't accept date format without leading zero
	strDate = Right("0" & DatePart("m",strDate), 2) _ 
		& "/" & Right("0" & DatePart("d",strDate), 2) _
		& "/" & DatePart("yyyy",strDate)
	'msgbox strDate
	strTime = FormatDateTime(Time, 4)
	With Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail")
		.WebEdit("name:=arrivalETADate").Set strDate
		.WebEdit("name:=arrivalETATime").Set strTime
		.WebEdit("name:=departureActualDate").Set strDate
		.WebEdit("name:=departureActualTime").Set strTime
		.WebEdit("name:=arrivalActualDate").Set strDate
		.WebEdit("name:=arrivalActualTime").Set strTime
	End With
	'	End Sub
	' ### Click Save button
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save']/following-sibling::*[1]").Click ' why click won't work?  10/1/2014 5:30pm - date format requires mm/dd/yyyy instead of windows default m/d/yyyy
	' Wait until the window closes by checking the Save button
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save']/following-sibling::*[1]").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote  strCoverStep & " - Loop times for the window to disappear: " & (intTimeOut - i)
	Wait 1 ' ###  try similar to waitforloading later *************************************
Loop

' ### check if the EZ button disappears
CheckEZ strCoverStep, 60

' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i, objPageMG
	Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub
