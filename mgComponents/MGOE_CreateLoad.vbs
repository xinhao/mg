﻿Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
'Const strBrowser = "chrome.exe"
Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")

Sub ReportStrComp(strExp, strAct, strDes)
	If StrComp(strAct, strExp, 1) = 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Expected:[" & strExp & "] Actual: [" & strAct & "]"
	End If
End Sub

Sub ReportInStr(strBig, strSmall, strDes)
	If InStr(1, strBig, strSmall, 1) <> 0 Then
		Reporter.Reportevent micPass, strDes,"checkpoint passed"
	Else
		Reporter.ReportEvent micFail, strDes, "checkpoint failed" & vbNewLine & "Small:[" & strSmall & "] Big: [" & strBig & "]"
	End If
End Sub

'
Function LoadDataTableFromResource(strDataTableFullPath, strSheetName)
   ' strDataTable = "[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\MGOE_CreateLoad.xls"
   ' strSheetName = "MGOE_CreateLoad"
	XLdatafile = PathFinder.Locate(strDataTableFullPath)
	
	If Len(XLdatafile) = 0 Then
		Reporter.ReportEvent micWarning, "Test Setup Error",  "The Datatable " & strDataTableFullPath & " is not found in QC"
		LoadDataTableFromResource = False
	Else
		DataTable.ImportSheet XLdatafile, strSheetName, strSheetName
		LoadDataTableFromResource = True
	End If
End Function
'--------------------------------------------------------------------------------
'Function Name: SaveDatatableToResource
'Description: The function allows run time datatable to be saved to QC Test
'             Resources module.
'             The resource name is specified in the function parameter and
'             the function will look for the resource name in QC Test Resources
'             module, then upload datatable to replace it.
'Created By: Hao Xin 
'Example: SaveDatatableToResource "test.xls"
'--------------------------------------------------------------------------------

Function SaveDatatableToResource(strResourceName, strSheetName)
    Dim qcConn, strTempFolder, intCount, strName
    ' Connect To QC
    Set qcConn = QCUtil.QCConnection

    ' Set Temp Folder
    strTempFolder = environment("SystemTempDir") 

    Set ResourceFactory = qcConn.QCResourceFactory
    Set ResourceList = ResourceFactory.NewList("")
    Set Resource = Nothing

	' Traverse all items to find the resource 
    For intCount = 1 To ResourceList.Count
		strName = ResourceList.Item(intCount).Name
		If UCase(strName) = UCase(strResourceName) Then
			Set Resource = ResourceList.Item(intCount)
		End If
    Next
    Set ResourceFactory = Nothing
    Set ResourceList = Nothing

    ' Assign the resource name (in QC11 it was blank)
	Resource.FileName = strResourceName
    
    ' Export Datatable to Temp Directory
    Datatable.ExportSheet strTempFolder & "\" & Resource.Filename, strSheetName

    Resource.Post
    ' Upload the exported datatable from temp folder to QC
    Resource.UploadResource strTempFolder, True
	Set Resource = Nothing
End Function


' ### Start
' ###Load DataTable from QC
If Not LoadDataTableFromResource("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\MGOE_CreateLoad.xls", "MGOE_CreateLoad") Then
	MsgBox "Please upload Excel Data Table MGOE_CreateLoad.xls first"
	ExitTest
End If

' ### Load parameter from component I/O parameters
' ### in MGOE_CreateLoad.xls, each row contains data for one scenario
' ### The parameter intSceRowNumber defines which row is to use
intSceRowNumber = Parameter("intSceRowNumber")
'msgbox intInput
If IsNumeric(intSceRowNumber)  Then
	intRow = intSceRowNumber
Else
	intRow = 0
	Reporter.ReportEvent micWarning, "Invalid Component Parameter", "Component MGEO_CreateLoad Input Parameter must be numeric" 
End If
'msgbox intRow
DataTable.GetSheet("MGOE_CreateLoad").SetCurrentRow intRow
'msgbox "Customer" & DataTable.Value("Customer")
'===========================

intExistingLoadNumberRow = DataTable.Value("ExistingLoadNumberRow")
If intExistingLoadNumberRow > 0 Then
	Parameter("strLoadNumber") = DataTable.GetSheet("MGOE_CreateLoad").GetParameter("ExistingLoadNumberRow").ValueByRow(intExistingLoadNumberRow)
Else
	objPageMG.Sync
	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebTable("innertext:=Add Transport").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click ' working
	'objPageMG.Frame("name:=Detail").WebTable("innertext:=Add Transport").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click ' working but can be simplified below:
	objPageMG.Frame("name:=Detail").WebElement("innertext:=Add Transport", "class:=x-btn-inner x-btn-inner-center").Click
	
	' BillTo Code
	strCustomer = DataTable.Value("Customer")
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=oidEnterprise").Click
	objWS.SendKeys strCustomer
	If Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=x-boundlist-list-ct x-unselectable", "innertext:=No Match Found.").exist(1) Then
		MsgBox  "BillTo Code " & strCustomer & " No Match Found. The test will stop. Please check test data."
		Reporter.ReportEvent micFail, "Data not found", "BillTo Code " & strCustomer & " No Match Found."
		ExitTest
	End If
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=x-boundlist-item x-boundlist-item-over").Click
	
	'Edit Event 1 (Pickup)
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").Image("css:=#event1 > div:nth-child(1) > a:nth-child(3) > img:nth-child(1)").Click ' working
	
	'Location code, type and select
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sLocationCode").Click
	' Location Code
	strShipper = DataTable.Value("Shipper")
	objWS.SendKeys strShipper
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebElement("xpath:=//*[starts-with(text(), '" & strShipper & "-SHP')]").Click

	If Browser("name:=Choose Contact").Page("title:=Choose Contact").Exist(1) Then ' detect a popup when there are multiple entries for a single location code
		Browser("name:=Choose Contact").Page("title:=Choose Contact").WebElement("xpath:=(//input)[last()]").Click
	End If
	
	'OK
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebButton("name:=OK").Click
	
	'Edit Event 2
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").Image("css:=#event2 > div:nth-child(1) > a:nth-child(3) > img:nth-child(1)").Click
	
	'Location code, type and select
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sLocationCode").Click
	strReceiver = DataTable.Value("Receiver")
	objWS.SendKeys strReceiver
	'Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sLocationCode").Set strReceiver ' not trigger the list
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebElement("xpath:=//*[starts-with(text(), '" & strReceiver & "-REC')]").Click
	
	If Browser("name:=Choose Contact").Page("title:=Choose Contact").Exist(1) Then ' detect a popup when there are multiple entries for a single location code
		Browser("name:=Choose Contact").Page("title:=Choose Contact").WebElement("xpath:=(//input)[last()]").Click
	End If
	' Verify Puckup Event field
'	strExpected = strName & ", "  & strCity
'	strActual = Browser("title:=https:.*").Page("name:=DateRangePopup").WebElement("name:=pickupEventitem1").GetROProperty("value")
	
	'ReportStrComp strExpected, strActual, "Pickup Event field check"
'	ReportInStr strActual, strExpected, "Pickup Event field check"
	'Browser("Browser").Page("Page").WebList("pickupEventItem1").Check CheckPoint("pickupEventItem1") @@ hightlight id_;_Browser("Browser").Page("Page").WebList("pickupEventItem1")_;_script infofile_;_ZIP::ssf3.xml_;_
	
	' Description
	strItemID = DataTable.Value("ItemID")
	strDescription = DataTable.Value("Description")
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sItemIdItem1").Set strItemID
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=sDescriptionItem1").Set strDescription
	' Planned Qty
	strQty = DataTable.Value("PlannedQTY")
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=fPlannedQuantityItem1").Set strQty
	' Palknned Wgt:
	strWgt = DataTable.Value("PlannedWeight")
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebEdit("name:=fPlannedWeightItem1").Set strWgt
	
	'OK
	Browser("title:=https:.*").Page("name:=DateRangePopup").WebButton("name:=OK").Click
	
	' Verify Qty, Wgt are populated on both events
	strExpected = FormatNumber(strQty, 1)  & " PLT"
	strActual = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemQuantity", "css:=#event1Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)").GetROProperty("innertext")
	ReportStrComp strExpected, strActual, "Qty1 check"
	
	strExpected = FormatNumber(strQty, 1)  & " PLT"
	strActual = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemQuantity", "css:=#event2Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(1)").GetROProperty("innertext")
	Call ReportStrComp(strExpected, strActual, "Qty2 check")
	
	strExpected = FormatNumber(strWgt, 0) & " lb"
	strActual = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemWeight", "css:=#event1Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3)").GetROProperty("innertext")
	Call ReportStrComp(strExpected, strActual, "Wgt1 check")
	
	strExpected = FormatNumber(strWgt, 0) & " lb"
	strActual = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebElement("class:=itemWeight", "css:=#event2Items > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(3)").GetROProperty("innertext")
	Call ReportStrComp(strExpected, strActual, "Wgt2 check")
	
	'Solicitor Code
	' css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > input:nth-child(1)
	' or xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[3]/td[2]/input
	' or css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
	strSolicitor = DataTable.Value("Solicitor")
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("xpath:=//td[.='Solicitor Code']/following-sibling::td[1]/input").Set strSolicitor
	
	'Operational Owner
	'css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2) > input:nth-child(1)
	'css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
	'xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[4]/td[2]/input
	strUserID = DataTable.Value("UserID")
	'Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=SR202744092191").Set strUserID ' not working in jbhunt4 already
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("xpath:=//td[.='Operational Owner']/following-sibling::td[1]/input").Set strUserID ' need to use better locator
	
	'Ship ID
	'css:=#scrollingBody2 > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2) > input:nth-child(1)
	'css:=html.x-quirks body#ext-gen1018.x-body.x-gecko form#form table tbody tr td div#scrollingBody2 div table tbody tr td.addScreenTD input.InputText
	'xpath:=/html/body/form/table/tbody/tr[2]/td[2]/div/div[1]/table/tbody/tr[5]/td[2]/input
	' guid first 8 
	Dim TypeLib
	Set TypeLib = CreateObject("Scriptlet.TypeLib")
	strUniqueID = Mid(TypeLib.Guid, 2, 8)
	strShipID = DataTable.Value("ShipID")
	If UCase(Trim(strShipID)) = UCase("Unique") Then
		Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("xpath:=//td[.='ShipID']/following-sibling::td[1]/input").Set strUniqueID
		DataTable.Value("oShipID") = strUniqueID
	Else
		Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("xpath:=//td[.='ShipID']/following-sibling::td[1]/input").Set strShipID
	End If
	
	'Load Type
	strLoadType = DataTable.Value("LoadType")
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebList("xpath:=//td[.='LoadType']/following-sibling::td[1]/select").Select strLoadType
	
	'Equipment
	strEquipment = DataTable.Value("Equipment")
	Select Case UCase(strEquipment)
	Case UCase("CHASSIS - 53 Chassis")
		strEquipValue = "CH"
	Case UCase("CONTAINER")
		strEquipValue = "CN"
	Case UCase("DRY VAN")
		strEquipValue = "TF"
	Case UCase("DRY VAN - 48 Van")
		strEquipValue = "TF:48"
	Case UCase("DRY VAN - 53 Van")
		strEquipValue = "TF:53"
	Case UCase("FLATBED")
		strEquipValue = "FT"
	Case UCase("FLATBED - 48 Flatbed")
		strEquipValue = "FT:48"
	Case UCase("FLATBED - 48 Stepdeck")
		strEquipValue = "SD:48"
	Case UCase("FLATBED - 53 Flatbed")
		strEquipValue = "TF:53"
	Case UCase("FLATBED - 53 Stepdeck")
		strEquipValue = "FT:53"
	Case UCase("FLATBED - Curtain Side")
		strEquipValue = "FR"
	Case UCase("FLATBED - Double Drop")
		strEquipValue = "DD"
	Case UCase("REEFER")
		strEquipValue = "RC"
	Case UCase("REEFER2") ' same label: deal with this later
		strEquipValue = "RT"
	Case UCase("REEFER - 48 Reefer")
		strEquipValue = "RC:48"
	Case UCase("REEFER - 48 Reefer2")
		strEquipValue = "RT:48"
	Case UCase("REEFER - 53 Reefer")
		strEquipValue = "RC:53"
	Case UCase("REEFER - 53 Reefer2")
		strEquipValue = "RT:53"
	Case UCase("TANKER")
		strEquipValue = "TN"
	Case UCase("FLATBED")
		strEquipValue = "FT"
	Case UCase("FLATBED")
		strEquipValue = "FT"
	Case UCase("TRUCK")
		strEquipValue = "V"
	End Select
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebCheckBox("name:=sSelectedEquipment", "value:=" & strEquipValue).Set "ON"
	
	'Special Instructions
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebEdit("name:=sSpecialInstructions").Set "My special instructions " & strUniqueID
	
	' Click Save button
	Browser("name:=TMS").Page("title:=TMS").Frame("name:=winaddMultiStopIFrame").WebButton("name:=Save").Click
	
	' load is successfully created
	Browser("name:=TMS").Dialog("regexpwndtitle:=Message from webpage").WinButton("regexpwndtitle:=OK").Click
	
	'Get load number
	strLoadNumber = Right(Browser("name:=TMS").Page("title:=TMS").WebElement("html id:=winLoadDetail_header_hd-textEl").GetROProperty("innertext"), 10)
	'msgbox strLoadNumber
	
	' Ways to save the load number:
	' passing parameter won't work on QTP11 due to issues reported
	' using data table for now
	
	' Load the datatable from QC
	'Dim fso
	'Set fso = CreateObject("Scripting.FileSystemObject")
	
'	DataTable.AddSheet "LoadNumber"
'	XLdatafile = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls")
'	
'	If Len(XLdatafile) = 0 Then
'		MsgBox("The Datatable is not found in QC")
'	Else
'		DataTable.ImportSheet XLdatafile, "LoadNumber", "LoadNumber"
'	End If
'	
'	intRow = datatable.GetSheet("LoadNumber").GetRowCount
'	datatable.GetSheet("LoadNumber").SetCurrentRow(intRow + 1)
	
	'Set TypeLib = CreateObject("Scriptlet.TypeLib")
	'strUniqueID = Mid(TypeLib.Guid, 2, 8)
	' Append the new load number to the datatable
	DataTable.Value("oLoadNumber") = strLoadNumber
	'DataTable.GetSheet("LoadNumber").GetParameter("LoadNumber").Value = strLoadNumber
	
	' Save the datatable to QC
	'DataTable.Export("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls") 'This is not working
	'SaveDatatableToResource "mg.xls", "LoadNumber"
	Reporter.ReportNote "Load " & strLoadNumber & " created at " & Date & " " & Time
	
	' ###Close the load window to avoid later element locating problems
	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[./span[@id='winLoadDetail_header_hd-textEl']]/following-sibling::div[5]/img")
	If objElement.Exist(1) Then
		objElement.Click
	End If

	' ### component I/O parameter Output Parameter value, need to test if it needs "Set"
	' Output parameter and DataTable saync to QC, the script only needs one of them. decide this when debugging
	Parameter("strLoadNumber") = strLoadNumber

	' ### Save DataTable
	SaveDataTableToResource "MGOE_CreateLoad.xls", "MGOE_CreateLoad"
	WriteLoadNumberSheet(strLoadNumber)
End If

Sub WriteLoadNumberSheet(strLoadNumber)
	DataTable.AddSheet "LoadNumber"
	XLdatafile = PathFinder.Locate("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls")
	
	If Len(XLdatafile) = 0 Then
		MsgBox("The Datatable is not found in QC, Exit Test...")
		ExitTest
	Else
		DataTable.ImportSheet XLdatafile, "LoadNumber", "LoadNumber"
	End If
	
	intRow = datatable.GetSheet("LoadNumber").GetRowCount
	datatable.GetSheet("LoadNumber").SetCurrentRow(intRow + 1)
	
	'Set TypeLib = CreateObject("Scriptlet.TypeLib")
	'strUniqueID = Mid(TypeLib.Guid, 2, 8)
	' Append the new load number to the datatable
	DataTable.GetSheet("LoadNumber").GetParameter("LoadNumber").Value = strLoadNumber
	
	' Save the datatablel to QC
	'DataTable.Export("[QualityCenter\Resources] Resources\Test-MIB\BPT Resources\mg.xls") 'This is not working
	SaveDatatableToResource "mg.xls", "LoadNumber"
	'Reporter.ReportNote "Load " & strLoadNumber & " created at " & Date & " " & Time
	
	' ###Close the load window to avoid later element locating problems
'	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[./span[@id='winLoadDetail_header_hd-textEl']]/following-sibling::div[5]/img")
'	If objElement.Exist(1) Then
'		objElement.Click
'	End If

End Sub
