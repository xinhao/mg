﻿'Sub Commented
' ###5:30pm 10/2/2014 
' ###something is wrong with Manage Quotes, the whole window is disabled at some point. debug tomorrow
' ###11:50am 10/3/2014 
' ### The problem was after  the filter popup closed, its elements are still Exist. that makes the script not exit from a Do loop
Dim strURL, objWS, objPageMG
'Const strBrowser = "iexplore.exe"
'Set objWS = CreateObject("WScript.Shell")
Set objPageMG = Browser("name:=TMS").Page("title:=TMS")
strCoverStep = "Assign Carrier Rate"
Set objBringBack = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Manage Quotes']")

objPageMG.Sync
' ###Assign Carrier Rate

' ### Look for Assign Carrier Rate with class='mg-bubble-title and htmltext='Assign Carrier Rate'
' ### Then look for its child and grandchild <a>/<img>
' ### msgbox objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[contains(concat(' ', normalize-space(@class), ' '), 'mg-bubble-title') and contains(text(), 'Assign Carrier Rate')]/a/img").GetROProperty("html id")
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Click

wait 2

' ### Locate THE money icon with correct value
Set objElementMoneyIcon = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//td[following-sibling::td[2][.='A ONE EXPRESS'] and following-sibling::td[6][.='TRUCK DRY VAN']]/div/img[@data-qtip='Enter Quote Charges']")
strSearchLabel = "Carrier Name (Begins):"
Set objElementFilterTextBox = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebEdit("xpath:=//td[.=normalize-space('" & strSearchLabel & "')]/following-sibling::td[1]/input") ''works
Set objElementFilterCARMA = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//span[.='From CARMA']")

Do While Not objElementMoneyIcon.Exist(10) ' ### When THE money icon doesn't exist,
	If Not objElementFilterTextBox.Exist(1) Then
		objElementFilterCARMA.Click
		i = 20
	   Do While (Not objElementFilterTextBox.Exist(1)) and i > 0
		   Wait 1
		   i = i - 1
	   Loop
	   If i <=0 Then
		   Reporter.ReportEvent micFail, "CARMA Filter window can't open", "Exit Test"
		   ExitTest
	   End If
	End If
	'Do While objElementFilterTextBox.Exist(1) ' ### the filter popup displays --- Debug note:
	' ### The loop fails to exit because objElementFilterTextBox.Exist(1) = True even when it's closed!

	' ### Enter "a" to text box
	objElementFilterTextBox.Set "a one"
	' ### locate the OK button on the Filter window. There are total 4 OK buttons (2 can be seen but the other 2 can't even be seen by eyes)
	'For i = 1 to 15
	'	Set objElement = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=(//span/span/span[.='OK'])[" & i & "]") ''total 4, the last one (4th) is the correct one when recording
	'	msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("innertext")
	'Next
	' ### OK button for the Filter popup
	' Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=(//span/span/span[.='OK'])[last()]") ' ### this is risky because OK buttons' order might change
	strSearchLabel = "Carrier Name (Begins):"
	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//div[./div/div/div/div/div/table/tbody/tr/td/label[.='" & strSearchLabel & "']]/following-sibling::div[2]/div/div/a/table/tbody/tr/td/span/span/span[.='OK']")
	'msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("innertext")
	objElement.Click

	' ### Search for a checkbox with the following:
	' ### SCAC: AON4
	' ### Carrier Name: A ONE EXPRESS
	' ### Carrier Id: AON4
	' ### Mode: TRUCK DRY VAN
	' ### Location: CERES, CA
	
	'  ### Locate the checkbox this way: find a <td> which has 2nd following sibling as 'AON4', 5th following sibling as 'TRUCK DRY VAN', then the found <td>'s <div> child
	'  ### <tr class="actualrow" style="height: 20px">
	'  ### 	<td class="actualcell sajcell-row-lines undefined saj-special x-grid-cell-special x-grid-cell-row-checker" style="width:24px;">
	'  ###   	<div class="x-grid-row-checker"> </div> ' ### the checkbox is here ###
	'  ###   </td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:50px;">
	'  ###   	<img src="/MercuryGate/images/appmenu/Carriers16.png">
	'  ###   </td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:65px;">AON4</td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:150px;">A ONE EXPRESS</td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:150px;">AON4</td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:135px;">TRUCK DRY VAN</td>
	'  ###   <td class="actualcell sajcell-row-lines undefined " style="width:150px;">CERES, CA</td>
	'  ### </tr>
	' ### Check 

	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//td[following-sibling::td[2][.='AON4'] and following-sibling::td[5][.='TRUCK DRY VAN']]/div") ' correct checkbox
	intTimeOut = 30
	Do While (Not objElement.Exist(1)) and intTimeOut > 0
		Wait 1
		intTimeOut = intTimeOut - 1
	Loop
	objElement.Click '### select the checkbox
	'msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("class")
	
	' ### locate the OK button on the Manage Quotes window. There are total 4 OK buttons (2 can be seen but the other 2 can't even see by eyes)
	'For i = 1 to 15
	'	Set objElement = Browser("title:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=(//span/span/span[.='OK'])[" & i & "]") ''total 4, the third one (second last) is the correct one when recording
	'	msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("innertext")
	'Next

	' ### find a better way to locate the Ok button later, the current way is risky ****************************************************************************************************************
	' ### OK button for ManageQuotes
	' ### The Ok button and Manage Quotes title belong to different frame (Default and winExtmanageSpotQuoteIFrame) therefore can't use the title to trace
	' ### using 'Add Carrier From CARMA Carrier List' to track
	'Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=(//span/span/span[.='OK'])[3]")
	Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//div[.//span[.='Add Carrier From CARMA Carrier List']]/following-sibling::div[4]/div/div/a/table/tbody/tr/td/span/span/span[.='OK']")
	'msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("innertext")
	intTimeOut = 30
	Do While (Not objElement.Exist(1)) and intTimeOut > 0
		Wait 1
		intTimeOut = intTimeOut - 1
	Loop
	objElement.Click
	Wait 2
	'objPageMG.Sync
	'	
	'Loop '### Completed filter, money icon should have appeared
	' ### in case no filter window and no correct money icon, it should be money icon for a wrong value
	' ### deal with this case later if it is possible to happen
Loop '### money icon exists with correct value

' off the screen, bring it back by clicking "ManageQuotes"status bar button twice
objBringBack.Click
Wait 1
objBringBack.Click	

objElementMoneyIcon.Click

'msgbox objElement.getroproperty("html tag") & " " & objElement.getroproperty("html id") & " " & objElement.GetROProperty("class")

'--------------------------- Step 18 enter values on Edit Quote Charges window 3:59 9/22/14
' Wait window opens
i = 20
strSearch = "Total Line Haul"
Do While (Not Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@value='" & strSearch & "']").Exist) and i > 0
   Wait 1
   i = i - 1
Loop

strRate1 = "215.12"
strRate3 = "0.35"

' ### Enter values - Total Line Haul
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeRate1']").Set strRate1
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeRateQualifier1']").Set "Flat Rate"
'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeQuantity1']").Set "1"

' ### Fuel Surcharge
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeRate3']").Set strRate3

' ### if it's already Per Mile, the subtotal will fail to update, so set to Flat Rate first regardless
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[./input[@name='priceSheetChargeRateQualifier3']]/following-sibling::td[1]/div").Click ' ### dropdown button
intTimeOut = 5
Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Flat Rate')]").Exist(1)) and intTimeOut > 0
	Wait 1
	intTimeOut = intTimeOut - 1
Loop
If  Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Flat Rate')]").Exist(1) Then
		'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Flat Rate')]").WaitProperty "visible", "True"
		Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Flat Rate')]").Click '
		Reporter.ReportNote "dropdown appeared"
Else
		Reporter.ReportNote "dropdown didn't appear"
End If

' ### Set to Per Mile
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[./input[@name='priceSheetChargeRateQualifier3']]/following-sibling::td[1]/div").Click ' ### dropdown button
intTimeOut = 5
Do While (Not Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Per Mile')]").Exist(1)) and intTimeOut > 0
	Wait 1
	intTimeOut = intTimeOut - 1
Loop
If  Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Per Mile')]").Exist(1) Then
		'Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Flat Rate')]").WaitProperty "visible", "True"
		Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div/ul/li[contains(text(), 'Per Mile')]").Click '
		Reporter.ReportNote "dropdown appeared"
Else
		Reporter.ReportNote "dropdown didn't appear"
		ExitTest
End If
'Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeQuantity3']").Set "21"
'End Sub

' ### Check Qty's are populated
'Qty1 = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//input[@name='priceSheetChargeQuantity1']").GetROProperty("value")
strDistance = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//td[./label[.='Distance:']]/following-sibling::td[1]/table/tbody/tr/td/input").GetROProperty("value")
strQty3 = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//input[@name='priceSheetChargeQuantity3']").GetROProperty("value")
If  CDbl(strQty3) > 1 Then
	Reporter.ReportEvent micPass, "Fuel Surcharge miles value check ", "Pass: " & strQty3
Else
	Reporter.ReportEvent micFail, "Fuel Surcharge miles value check ", "Fail: " & strQty3
End If
'### Check calculation
strRate3 = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebEdit("xpath:=//input[@name='priceSheetChargeRate3']").GetROProperty("value")
'strAmt1 = Browser("title:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//table[./tbody/tr/td/table/tbody/tr/td/input[@name='priceSheetChargeQuantity1']]/following-sibling::table[1]/tbody/tr/td/div[2]").GetROProperty("innertext")
strAmt3 = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//table[./tbody/tr/td/table/tbody/tr/td/input[@name='priceSheetChargeQuantity3']]/following-sibling::table[1]/tbody/tr/td/div[2]").GetROProperty("innertext")
strAmtTotal = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//table[./tbody/tr/td/div[.='Total:']]/following-sibling::table[1]/tbody/tr/td[2]/div").GetROProperty("innertext")
If CDbl(strRate3 * strQty3) = CDbl(strAmt3) Then
	Reporter.ReportEvent micPass, "Verify amount", "Pass" & strRate3 & " * " & strQty3 & " = " & strAmt3
Else
	Reporter.ReportEvent micFail, "Verify amount", "Fail " & strRate3 & " * " & strQty3 & " <> " & strAmt3 & " Rate*Qty: " & strRate3*strQty3
End If

' ### Save button
Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//span/span/span[.='Save']").Click'
'Wait 2
'Sub Commented2
' ### Select the select checkbox with the right value
Set objElementCheckBox = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//td[following-sibling::td[1][.='A ONE EXPRESS'] and following-sibling::td[5][.='TRUCK DRY VAN']]/div/img")
Do While Not objElementCheckBox.Exist(1)
	Set objElementCheckBox = Browser("name:=TMS").Page("title:=TMS").Frame("name:=winExtmanageSpotQuoteIFrame").WebElement("xpath:=//td[following-sibling::td[1][.='A ONE EXPRESS'] and following-sibling::td[5][.='TRUCK DRY VAN']]/div/img")
	Wait 1
Loop
objElementCheckBox.Click
' ### Close Manage Quotes window
Set objElement = Browser("name:=TMS").Page("title:=TMS").Frame("name:=Detail").WebElement("xpath:=//div[./span[normalize-space(.)='Manage Quotes']]/following-sibling::div[5]/img")
If objElement.Exist(1) Then
	objElement.Click
End If

' ### check if the EZ button disappears
CheckEZ strCoverStep, 60
'End Sub
' ### Check if the EZ Click icon disappears
Sub CheckEZ(strCoverStep, intTimeOut)
	'intTimeOut = 60
	Dim i
	i = intTimeOut
	Do While (objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1)) and i >0
		Wait 1
		i = i -1
	Loop
	Reporter.ReportNote strCoverStep & " - Loop times for EZ icon to disappear: " & (intTimeOut - i)
	If objPageMG.Frame("name:=Detail").WebElement("xpath:=//div[@class='mg-bubble-title' and normalize-space(.)='" & strCoverStep & "']/a/img").Exist(1) Then
		Reporter.ReportEvent micWarning, strCoverStep, "The item is still showing up"
	Else
		Reporter.ReportEvent micPass, strCoverStep, "The item disappears as expected"
	End If
End Sub
