# README #

Mercury Gate automation project

### What is this repository for? ###

* QTP Business Process Test framework
* Version QTP 11.00
* Folders:
    * **generalScript**: store some code snippets that might be useful elsewhere
    * **mgArchives**: for stuff not using anymore but might be useful later or again
    * **mgComponents**: backup scripts, e.g. [QualityCenter] Components\MIB\MG\ or other QC folders specified in ReadComponents.vbs arguments. Run ReadComponent.vbs for more information.
    * **mgDocs**: supporting documents created by me for the project
    * **mgOthersDocs**: supporting documentation from other teams or team members for the project
    * **mgResources**: QC Resources files, sync up periodically manually
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Run ReadBPScript.vbs periodically to backup script changes, see steps below.
* Run QCResource.vbs to backup or upload resource files from local drive
* Think about how to do the following:
    1. restore them back to QC - implement "u" command for ReadComponent.vbs
    1. explore ways to branch the work

### Backup steps:
1. Manully download files listed in mgResources folder from QC Resources module
1. There is a batch file created
1. In Windows command prompt:

``` bat
H:\mg>C:\Windows\SysWOW64\cscript ReadComponent.vbs s "Components\MG\anyFolder" "H:\mg\mgComponents\" "NOT(~* X~ OR ~* LOCKED~)"
H:\mg>C:\Windows\SysWOW64\cscript QCResource.vbs s "H:\mg\mgResources\ mg*
H:\mg>git-bash
```
``` bash
jisqhx3@WIN7-7160 /h/mg (master)
$ git status
$ git add -A .
$ git commit -m"commit comments"
$ git push
```

The batch QCfile.bat can perform the backup as well (without git part)

* List the backup files with path:
`H:\mg\QCfile l`
* Download backup files from QC to local drive:
`H:\mg\QCfile s`
* Upload only MGOE_CreateLoad.xls:
`H:\mg\QCfile u`